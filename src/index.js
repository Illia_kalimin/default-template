import './scss/style.scss'

window.Vue = require('vue')

import HelloWorld from './js/components/HelloWorld.vue'
Vue.component('HelloWorld', HelloWorld)
new Vue({}).$mount('#app')

window.addEventListener('load', function () {
    document.body.classList.remove('loading')
});